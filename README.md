## GitHisViz - Git History Visualizer

GitHisViz is a powerful companion tool designed to simplify the navigation of a GitLab repository's history. You can access the app here: [https://githisviz.vercel.app/](https://githisviz.vercel.app/)

When you're tracking data with Git History Keeper, reviewing changes over time directly in the project repository can be cumbersome and slow. GitHisViz addresses this challenge by offering a user-friendly interface to explore your repository's history effortlessly.

### How GitHisViz Works

GitHisViz loads your repository and its commits, providing intuitive navigation to view data changes across different points in time. With interactive visualizations, it helps you understand how your data evolves over the course of your project.

#### Key Features

1. **Effortless Navigation**: Seamlessly browse through your repository's history and track data changes with ease.
2. **Compare data across different commits**: see how data differes at different point in time.
3. **Interactive Visualization**: Visual representations help you grasp data transformations over time at a glance.
