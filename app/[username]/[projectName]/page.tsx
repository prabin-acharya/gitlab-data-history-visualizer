"use client";

import DataTable from "@/app/components/DataTable";
import { useEffect, useState } from "react";
import { IoCloseSharp } from "react-icons/io5";

import VisualizeChart from "@/app/components/VisualizeChart";
import Image from "next/image";
import { GrNext, GrPrevious } from "react-icons/gr";
import SyntaxHighlighter from "react-syntax-highlighter";

import LoadingSpinner from "@/app/components/LoadingSpinner";
import { useRouter } from "next/navigation";
import { docco } from "react-syntax-highlighter/dist/esm/styles/hljs";

interface RepoItem {
  id: string;
  name: string;
  type: "tree" | "blob";
  path: string;
  mode: string;
}

interface Commit {
  id: string;
  author_name: string;
  created_at: Date;
  commiter_name: string;
  message: string;
  title: string;
  parents_id: string[];
}

interface JsonObject {
  [key: string]: any;
}

export default function Page({
  params,
}: {
  params: { username: string; projectName: string };
}) {
  const [repoFiles, setRepoFiles] = useState<RepoItem[]>([]);
  const [fileCommits, setFileCommits] = useState<Commit[]>([]);

  const [selectedFile, setSelectedFile] = useState("");
  const [selectedCommit, setSelectedCommit] = useState<Commit | undefined>();
  const [selectedCommit2, setSelectedCommit2] = useState<Commit | undefined>();
  const [showCompareTab, setShowCompareTab] = useState(false);

  const [selectedJsonProperty, setSelectedJsonProperty] = useState<
    string | undefined
  >();

  const [fileContent, setFileContent] = useState<undefined | string>();
  const [fileContent2, setFileContent2] = useState<undefined | string>();
  const [fileContentLoading, setFileContentLoading] = useState(false);
  const [fileContentLoading2, setFileContentLoading2] = useState(false);

  const [selectedFileContentHistory, setSelectedFileContentHistory] = useState<
    {
      date: Date;
      commitId: string;
      content: string;
      commitMessage: string;
    }[]
  >();

  const [showFullFileContent, setShowFullFileContent] = useState(true);

  //
  const [showVisualizationMenu, setShowVisualizationMenu] = useState(false);
  const [visualizationPlotYPath, setVisualizationPlotYPath] = useState("");
  const [showPlot, setShowPlot] = useState(false);
  const [plotSubFieldPath, setPlotSubFieldPath] = useState("");

  const router = useRouter();

  const fetchFileContentHistory = async () => {
    try {
      const urlEncodedSelectedFilePath = encodeURIComponent(selectedFile);

      const promises = fileCommits.map(async (commit) => {
        const response = await fetch(
          `https://gitlab.com/api/v4/projects/${params.username}%2F${params.projectName}/repository/files/${urlEncodedSelectedFilePath}?ref=${commit.id}`
        );
        const data = await response.json();
        return {
          date: commit.created_at,
          commitId: commit.id,
          content: atob(data.content || ""),
          commitMessage: commit.message,
        };
      });

      const resolvedContents = await Promise.all(promises);
      console.log(
        resolvedContents,
        "$$$$$$$-----------------------------------------"
      );
      setSelectedFileContentHistory(resolvedContents);
    } catch (error) {
      console.error("Error fetching file content for commits:", error);
    }
  };

  useEffect(() => {
    const fetchFileCommitHistory = async () => {
      if (!selectedFile) return;
      try {
        const response = await fetch(
          `https://gitlab.com/api/v4/projects/${params.username}%2F${params.projectName}/repository/commits?path=${selectedFile}`
        );

        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }

        const data = await response.json();
        setFileCommits(data.reverse());
        setSelectedCommit(data[0]);
        // fetchFileContent();
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchFileCommitHistory();
  }, [selectedFile, params.username, params.projectName]);

  useEffect(() => {
    const fetchFileContent = async () => {
      if (!selectedFile || !selectedCommit?.id) return;
      try {
        setFileContentLoading(true);
        const urlEncodedSelectedFilePath = encodeURIComponent(selectedFile);
        const response = await fetch(
          `https://gitlab.com/api/v4/projects/${params.username}%2F${params.projectName}/repository/files/${urlEncodedSelectedFilePath}?ref=${selectedCommit.id}`
        );

        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }

        const data = await response.json();
        const decodedFileContent =
          atob(data.content) || JSON.stringify({ message: "No File Content" });
        setFileContent(decodedFileContent);
        setFileContentLoading(false);
      } catch (error) {
        setFileContentLoading(false);
        console.error("Error fetching data:", error);
      }
    };
    fetchFileContent();
  }, [selectedCommit?.id, selectedFile, params.projectName, params.username]);

  useEffect(() => {
    const fetchFileContent = async () => {
      if (!selectedFile || !selectedCommit2?.id) return;
      try {
        setFileContentLoading2(true);
        const urlEncodedSelectedFilePath = encodeURIComponent(selectedFile);
        const response = await fetch(
          `https://gitlab.com/api/v4/projects/${params.username}%2F${params.projectName}/repository/files/${urlEncodedSelectedFilePath}?ref=${selectedCommit2.id}`
        );

        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }

        const data = await response.json();
        const decodedFileContent =
          atob(data.content) || JSON.stringify({ message: "No File Content" });
        setFileContent2(decodedFileContent);
        setFileContentLoading2(false);
      } catch (error) {
        setFileContentLoading2(false);
        console.error("Error fetching data:", error);
      }
    };
    fetchFileContent();
  }, [selectedCommit2?.id, selectedFile, params.projectName, params.username]);

  useEffect(() => {
    const fetchRepoFiles = async () => {
      console.log("---------------");
      try {
        const response = await fetch(
          `https://gitlab.com/api/v4/projects/${params.username}%2F${params.projectName}/repository/tree?recursive=true`
        );

        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }

        const data = await response.json();
        const repoFilesPaths = data.filter(
          (file: any) => file.path.slice(-5) == ".json"
        );

        if (!selectedFile) setSelectedFile(repoFilesPaths[0].path);
        setRepoFiles(repoFilesPaths);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    if (repoFiles.length == 0) fetchRepoFiles();
  }, [repoFiles, selectedFile, params.projectName, params.username]);

  //

  const [selectedFileSubfieldContent, setSelectedFileSubfieldContent] =
    useState<any>();
  const [selectedFileSubfieldContent2, setSelectedFileSubfieldContent2] =
    useState<any>();
  const [selectedFileSubfieldPath, setSelectedFileSubfieldPath] = useState("");
  console.log(selectedFileSubfieldContent);

  useEffect(() => {
    if (!fileContent) return;

    const pathArray = selectedFileSubfieldPath.split(".");
    let currentValue: any = JSON.parse(fileContent as string);

    for (const key of pathArray) {
      if (/\[\d+\]/.test(key)) {
        // Check if key contains array indexing
        const [arrayKey, index] = key.split("[");
        const idx = parseInt(index.slice(0, -1), 10);
        if (
          currentValue.hasOwnProperty(arrayKey) &&
          Array.isArray(currentValue[arrayKey])
        ) {
          currentValue = currentValue[arrayKey][idx];
        } else {
          currentValue = undefined;
          setShowFullFileContent(true);
          break;
        }
      } else if (currentValue.hasOwnProperty(key)) {
        currentValue = currentValue[key];
      } else {
        currentValue = undefined;
        setShowFullFileContent(true);
        break;
      }
    }

    setSelectedFileSubfieldContent(currentValue);
  }, [fileContent, selectedFileSubfieldPath]);

  useEffect(() => {
    if (!fileContent2) return;

    const pathArray = selectedFileSubfieldPath.split(".");
    let currentValue2: any = JSON.parse(fileContent2 as string);

    for (const key of pathArray) {
      if (/\[\d+\]/.test(key)) {
        // Check if key contains array indexing
        const [arrayKey, index] = key.split("[");
        const idx = parseInt(index.slice(0, -1), 10);
        if (
          currentValue2.hasOwnProperty(arrayKey) &&
          Array.isArray(currentValue2[arrayKey])
        ) {
          currentValue2 = currentValue2[arrayKey][idx];
        } else {
          currentValue2 = undefined;
          setShowFullFileContent(true);
          break;
        }
      } else if (currentValue2.hasOwnProperty(key)) {
        currentValue2 = currentValue2[key];
      } else {
        currentValue2 = undefined;
        setShowFullFileContent(true);
        break;
      }
    }

    setSelectedFileSubfieldContent2(currentValue2);
  }, [fileContent2, selectedFileSubfieldPath]);

  const handlePathInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const path = e.target.value.trim();
    setSelectedFileSubfieldPath(path);
    const pathArray = path.split(".");
    let currentValue: any = JSON.parse(fileContent as string);
    let currentValue2: any = JSON.parse(
      fileContent2 || (`{"error":"No file content"}` as string)
    );

    for (const key of pathArray) {
      if (/\[\d+\]/.test(key)) {
        // Check if key contains array indexing
        const [arrayKey, index] = key.split("[");
        const idx = parseInt(index.slice(0, -1), 10);
        if (
          currentValue.hasOwnProperty(arrayKey) &&
          Array.isArray(currentValue[arrayKey])
        ) {
          currentValue = currentValue[arrayKey][idx];
        } else {
          currentValue = undefined;
          setShowFullFileContent(true);
          break;
        }
      } else if (currentValue.hasOwnProperty(key)) {
        currentValue = currentValue[key];
      } else {
        currentValue = undefined;
        setShowFullFileContent(true);
        break;
      }
    }

    for (const key of pathArray) {
      if (/\[\d+\]/.test(key)) {
        // Check if key contains array indexing
        const [arrayKey, index] = key.split("[");
        const idx = parseInt(index.slice(0, -1), 10);
        if (
          currentValue2.hasOwnProperty(arrayKey) &&
          Array.isArray(currentValue2[arrayKey])
        ) {
          currentValue2 = currentValue2[arrayKey][idx];
        } else {
          currentValue2 = undefined;
          setShowFullFileContent(true);
          break;
        }
      } else if (currentValue2.hasOwnProperty(key)) {
        currentValue2 = currentValue2[key];
      } else {
        currentValue2 = undefined;
        setShowFullFileContent(true);
        break;
      }
    }

    setSelectedFileSubfieldContent(currentValue);
    setSelectedFileSubfieldContent2(currentValue2);
  };

  if (!fileContent)
    return (
      <div className="min-h-screen w-full flex">
        <div className="m-auto">
          <LoadingSpinner />
        </div>
      </div>
    );

  console.log(selectedFileSubfieldContent, "^^^^");

  const handleVisualizationPlotYPathChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const path = e.target.value.trim();
    setPlotSubFieldPath(path);
    const pathArray = path.split(".");
    let currentValue: any = JSON.parse(fileContent as string);

    for (const key of pathArray) {
      if (/\[\d+\]/.test(key)) {
        // Check if key contains array indexing
        const [arrayKey, index] = key.split("[");
        const idx = parseInt(index.slice(0, -1), 10);
        if (
          currentValue.hasOwnProperty(arrayKey) &&
          Array.isArray(currentValue[arrayKey])
        ) {
          currentValue = currentValue[arrayKey][idx];
        } else {
          currentValue = undefined;
          break;
        }
      } else if (currentValue.hasOwnProperty(key)) {
        currentValue = currentValue[key];
      } else {
        currentValue = undefined;
        break;
      }
    }

    setVisualizationPlotYPath(currentValue);
  };

  console.log(showCompareTab, "%%$$$$$$$$$$$$");

  return (
    <main className="flex min-h-screen  flex-col">
      {/* Header */}
      <div className="bg-gray-700 px-4 py-2 text-white">
        <span
          className=" text-logo font-bold text-sky-500 block mb-1 cursor-pointer"
          onClick={() => {
            router.push("/");
          }}
        >
          GitHisViz
          <span className="text-xxs p-1 ml-2 rounded-md bg-yellow-600 text-black">
            ALPHA
          </span>
        </span>
        <h1 className="font-semibold text-white text-xl mb-2 underline">
          {params.username} / {params.projectName}
        </h1>

        {/* Data Files */}
        <div>
          <span>Data File:</span>
          <select
            className="mx-2 text-black bg-gray-300"
            value={selectedFile}
            onChange={(event) => setSelectedFile(event.target.value)}
          >
            {repoFiles.map((file, i) => (
              <option key={i} value={file.path}>
                {file.path}
              </option>
            ))}
          </select>

          <input
            type="text"
            placeholder="enter subfield path"
            className=" px-2 outline-none text-whit bg-gray-300 text-black"
            onChange={(e) => handlePathInputChange(e)}
          />

          <br />

          <button
            onClick={() => {
              setShowVisualizationMenu(true);
              fetchFileContentHistory();
            }}
            className="px-4 py-1 border rounded bg-gray-600 text-white my-2 mt-3"
          >
            Visualize History
          </button>
        </div>

        {showVisualizationMenu && (
          <>
            <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-50 z-50">
              <div className="bg-white text-black rounded w-96 h-fit flex flex-col px-6 py-4 pb-16">
                <div className="flex flex-row-reverse">
                  <IoCloseSharp
                    onClick={() => {
                      setShowVisualizationMenu(false);
                      setShowPlot(false);
                    }}
                    className="cursor-pointer"
                  />
                </div>
                <h2 className="text-2xl font-semibold mb-4">
                  Visualize History
                </h2>
                <div className="flex flex-row">
                  <label className="">X-axis: </label>
                  <input
                    value={"commit time"}
                    className="border outline-none border-gray-700 rounded ml-2 px-2 bg-gray-200 text-gray-600 cursor-default"
                  />
                </div>
                <div className="flex flex-row mt-2 w-full">
                  <label className="">Y-axis: </label>
                  <input
                    type="text"
                    placeholder="enter subfield path"
                    // value={visualizationPlotYPath}
                    onChange={(e) => handleVisualizationPlotYPathChange(e)}
                    className="border outline-none border-slate-500 rounded ml-2 px-2 bg-gray-100 w-fit"
                  />
                </div>
                <button
                  onClick={() => {
                    setShowVisualizationMenu(false);
                    setShowCompareTab(false);
                    setShowPlot(true);
                  }}
                  className="mt-3 border bg-gray-700 text-white rounded py-1"
                >
                  Plot History
                </button>
              </div>
            </div>
          </>
        )}

        {/* Commits */}
        <div>{/* <span>Commits:</span> */}</div>

        {/* Json properties */}
        {/* <span>Select a Property:</span> */}
        {/* <div className="py-2">
          {Object.keys(fileContentJSON).map((key) => (
            <span key={key} className="p-2">
              <button
                onClick={() => setSelectedJsonProperty(key)}
                className={`${
                  selectedJsonProperty == key && "bg-slate-400"
                } px-2 rounded`}
              >
                {key}
              </button>
            </span>
          ))}
        </div> */}
      </div>

      {/* Content */}
      <div className="flex flex-grow  h-full bg-fileContentBg">
        <div className="flex flex-col w-1/2 border-2 ">
          {/* file header */}
          <div className="px-2 py-1 bg-slate-200 items-center justify-center">
            <span className="font-semibold">{selectedFile}</span> at
            <select
              className="ml-2 outline-none"
              value={selectedCommit?.id}
              onChange={(event) =>
                setSelectedCommit(
                  fileCommits.find((commit) => commit.id == event.target.value)
                )
              }
            >
              {fileCommits.map((commit, i) => (
                <option key={i} value={commit.id} className="px-1 py-1">
                  {formattedDateTime(new Date(commit.created_at))}
                </option>
              ))}
            </select>
            <GrPrevious
              onClick={() => {
                if (selectedCommit && fileCommits.indexOf(selectedCommit) !== 0)
                  setSelectedCommit(
                    fileCommits[fileCommits.indexOf(selectedCommit) - 1]
                  );
              }}
              className="inline cursor-pointer ml-4"
            />
            <GrNext
              onClick={() => {
                if (
                  selectedCommit &&
                  fileCommits.indexOf(selectedCommit) !== fileCommits.length - 1
                )
                  setSelectedCommit(
                    fileCommits[fileCommits.indexOf(selectedCommit) + 1]
                  );
              }}
              className="inline cursor-pointer mx-4 "
            />
            {selectedFileSubfieldContent && (
              <div className="inline bg-slate-400 px-2 py-1 rounded cursor-pointer">
                <span
                  onClick={() => setShowFullFileContent(false)}
                  className={`${
                    showFullFileContent ? "" : "bg-white"
                  } px-2 rounded`}
                >
                  data.{selectedFileSubfieldPath}
                </span>
                <span
                  onClick={() => setShowFullFileContent(true)}
                  className={`${
                    showFullFileContent ? "bg-white" : ""
                  } px-2 rounded w-fit`}
                >
                  file content
                </span>
              </div>
            )}
            {!showCompareTab && (
              <span
                onClick={() => {
                  setShowCompareTab(true);
                  setSelectedCommit2(selectedCommit);
                  setShowPlot(false);
                }}
                className="ml-3 px-4 py-1 bg-gray-400 rounded cursor-pointer"
              >
                Compare
              </span>
            )}
          </div>
          {fileContentLoading ? (
            <div className=" bg-fileContentBg h-full flex flex-grow items-center justify-center">
              <LoadingSpinner />
            </div>
          ) : (
            <>
              {isImageUrl(selectedFileSubfieldContent) &&
              !showFullFileContent ? (
                <Image
                  src={selectedFileSubfieldContent}
                  width={550}
                  height={550}
                  alt="hello"
                  className="m-auto"
                />
              ) : (
                <SyntaxHighlighter
                  language="json"
                  style={docco}
                  wrapLongLines={true}
                >
                  {fileContent && showFullFileContent
                    ? fileContent
                    : JSON.stringify(selectedFileSubfieldContent, null, 2) ||
                      "Hello"}
                  {/* {fileContent} */}
                  {/* {} */}
                </SyntaxHighlighter>
              )}
            </>
          )}
        </div>

        {/* Compare file*/}
        {/* Compare file*/}
        {showCompareTab && (
          <div className="flex flex-col w-1/2 border-2 ">
            {/* file header */}
            <div className="px-2 py-1 bg-slate-200 flex items-center justify-between">
              <div>
                <span className="font-semibold">{selectedFile}</span> at
                <select
                  className="ml-2  outline-none"
                  value={selectedCommit2?.id}
                  onChange={(event) =>
                    setSelectedCommit2(
                      fileCommits.find(
                        (commit) => commit.id == event.target.value
                      )
                    )
                  }
                >
                  {fileCommits.map((commit, i) => (
                    <option key={i} value={commit.id} className="px-1 py-1">
                      {formattedDateTime(new Date(commit.created_at))}
                    </option>
                  ))}
                </select>
                <GrPrevious
                  onClick={() => {
                    if (
                      selectedCommit2 &&
                      fileCommits.indexOf(selectedCommit2) !== 0
                    )
                      setSelectedCommit2(
                        fileCommits[fileCommits.indexOf(selectedCommit2) - 1]
                      );
                  }}
                  className="inline cursor-pointer ml-4"
                />
                <GrNext
                  onClick={() => {
                    if (
                      selectedCommit2 &&
                      fileCommits.indexOf(selectedCommit2) !==
                        fileCommits.length - 1
                    )
                      setSelectedCommit2(
                        fileCommits[fileCommits.indexOf(selectedCommit2) + 1]
                      );
                  }}
                  className="inline cursor-pointer mx-4 "
                />
              </div>
              <IoCloseSharp
                onClick={() => setShowCompareTab(false)}
                className="inline cursor-pointer"
              />
            </div>
            {fileContentLoading2 ? (
              <div className=" bg-fileContentBg h-full flex flex-grow items-center justify-center">
                <LoadingSpinner />
              </div>
            ) : (
              <>
                {isImageUrl(selectedFileSubfieldContent2) &&
                !showFullFileContent ? (
                  <Image
                    src={selectedFileSubfieldContent2}
                    width={550}
                    height={550}
                    alt="hello"
                    className="m-auto"
                  />
                ) : (
                  <SyntaxHighlighter
                    language="json"
                    style={docco}
                    wrapLongLines={true}
                  >
                    {fileContent2 && showFullFileContent
                      ? fileContent2
                      : JSON.stringify(selectedFileSubfieldContent2, null, 2) ||
                        "Hello"}
                    {/* {fileContent} */}
                    {/* {} */}
                  </SyntaxHighlighter>
                )}
              </>
            )}
          </div>
        )}

        {/* b */}
        {/* b */}
        {/* b */}

        {showPlot && (
          <div className="m-auto">
            <VisualizeChart
              data={selectedFileContentHistory}
              subFieldPath={plotSubFieldPath}
            />
          </div>
        )}

        {/* Table */}
        {/* {Array.isArray(fileContentSelectedProperty) && (
          <DataTable data={fileContentSelectedProperty} />
        )} */}

        {/* if Image */}
      </div>
    </main>
  );
}

function isImageUrl(url: any) {
  return /\.(jpeg|jpg|gif|png)$/i.test(url);
}

function formattedDateTime(date: Date) {
  return `${date?.getFullYear()}-${(date?.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${date.getDate().toString().padStart(2, "0")} ${date
    .getHours()
    .toString()
    .padStart(2, "0")}:${date.getMinutes().toString().padStart(2, "0")}`;
}
