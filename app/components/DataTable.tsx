export default function DataTable({ data }: { data: any }) {
  const columnNames = data.length > 0 ? Object.keys(data[0]) : [];
  console.log(data, "##");

  return (
    <div className="flex flex-col">
      <div className=" overflow-x-auto">
        <div className="p-1.5 min-w-full inline-block align-middle">
          <div className="border-2 rounded-lg overflow-hidden ">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="">
                <tr className="bg-gray-200">
                  {columnNames.map((columnName) => (
                    <th
                      scope="col"
                      key={columnName}
                      className="px-6 py-3 text-start text-sm font-semibold text-gray-500 uppercase"
                    >
                      {columnName}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-200 ">
                {data &&
                  data?.map((row: any, index: number) => (
                    <tr key={index} className="hover:bg-gray-100">
                      {columnNames.map((columnName) => (
                        <td
                          key={columnName}
                          className=" px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800 "
                        >
                          {row[columnName]}
                        </td>
                      ))}
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}
