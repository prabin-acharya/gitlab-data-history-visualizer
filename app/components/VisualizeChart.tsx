import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

export default function VisualizeChart({
  data,
  subFieldPath,
}: {
  data: any;
  subFieldPath: string;
}) {
  const data2 = [
    { time: "2024-02-26T08:00:00", aqi: 25 },
    { time: "2024-02-26T09:00:00", aqi: 28 },
    { time: "2024-02-26T10:00:00", aqi: 32 },
    // Add more data points as needed
  ];

  console.log(data);
  const cleanedData = data?.map((item: any) => {
    const content = JSON.parse(item.content || JSON.stringify({ e: "dot" }));
    return {
      date: convertToReadableDate(item?.date),
      value: content,
    };
  });

  console.log(cleanedData);

  const filteredData = cleanedData?.filter((data: any) => {
    if (data?.value?.list && data.value.list[0]?.components?.co) return data;
  });

  const filteredData2 = filteredData?.map((data: any) => {
    return {
      date: formattedDateTime(new Date(data.date)),
      value: data.value.list[0]?.components?.co,
    };
  });

  // console.log(filteredData);

  const parts = subFieldPath.split(".");
  const valueName = parts[parts.length - 1];

  const rawData = data;
  const dataXY = rawData.map((file: any) => {
    const path = subFieldPath;
    const pathArray = path.split(".");
    let currentValue: any = JSON.parse(file.content as string);

    for (const key of pathArray) {
      if (/\[\d+\]/.test(key)) {
        // Check if key contains array indexing
        const [arrayKey, index] = key.split("[");
        const idx = parseInt(index.slice(0, -1), 10);
        if (
          currentValue.hasOwnProperty(arrayKey) &&
          Array.isArray(currentValue[arrayKey])
        ) {
          currentValue = currentValue[arrayKey][idx];
        } else {
          currentValue = undefined;
          break;
        }
      } else if (currentValue.hasOwnProperty(key)) {
        currentValue = currentValue[key];
      } else {
        currentValue = undefined;
        break;
      }
    }

    let obj: any = {
      formattedDate: formattedDateTime(new Date(file.date)),
      date: new Date(file.date),
    };
    obj[valueName] = currentValue;

    return obj;
  });

  const dataXYSorted = dataXY.sort((a: any, b: any) => a.date - b.date);

  return (
    <div className="">
      {/* <p>Visualization</p> */}
      <LineChart
        width={800}
        height={550}
        data={dataXYSorted}
        margin={{ top: 20, right: 30, left: 20, bottom: 10 }}
      >
        {/* <CartesianGrid strokeDasharray="3 3" /> */}
        <XAxis dataKey="formattedDate" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="natural" dataKey={valueName} stroke="#8884d8" />
      </LineChart>
    </div>
  );
}

function convertToReadableDate(dateString: string) {
  const date = new Date(dateString);
  //   const options =
  return date.toLocaleDateString("en-US");
}

function formattedDateTime(date: Date) {
  return `${date?.getFullYear()}-${(date?.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${date.getDate().toString().padStart(2, "0")} ${date
    .getHours()
    .toString()
    .padStart(2, "0")}:${date.getMinutes().toString().padStart(2, "0")}`;
}
