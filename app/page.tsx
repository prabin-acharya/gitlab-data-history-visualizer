"use client";

import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useState } from "react";

export default function Home() {
  const [inputValue, setInputValue] = useState<string>("");

  const router = useRouter();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  const handleClick = () => {
    router.push(`/${inputValue}`);
  };

  return (
    <main className="flex min-h-screen flex-col items-center justify-between bg-slate-200">
      <div className="flex flex-col m-auto border border-gray-400 w-1/3 min-w-96 text-center py-20 px-6 rounded-md shadow-lg">
        <div className="pb-10">
          <h1 className="text-3xl font-bold text-sky-600 inline">
            GitHisViz
            <span className="text-xxs p-1 ml-2 rounded-md bg-yellow-400 text-black">
              ALPHA
            </span>
          </h1>
          <p>Git History Visualizer</p>
          <p>
            a tool for interacting with
            <Link
              href={"https://gitlab.com/prabin-acharya/gitlog-w-gitlab"}
              className=" rounded px-2 text-orange-700  py-1 underline"
            >
              GitLog-w-gitlab
            </Link>{" "}
            repos
          </p>

          {/* <p>a tool for  visuaGit Scraping</p> */}
        </div>
        <div className="flex flex-col text-left py-2">
          <label className="mb-2">Enter gitlab repo path:</label>
          <div className="flex justify-between  overflow-hidden">
            <input
              type="text"
              value={"https://gitlab.com/"}
              placeholder="username/repo-name"
              className="text-gray-500 rounded-l bg-gray-100 outline-none p-2 min-w-36 border border-orange-600 focus:border-black outline-gray-500 w-fit"
            />
            <input
              type="text"
              value={inputValue}
              onChange={handleInputChange}
              placeholder="username/repo-name"
              className="p-2 border w-fit border-orange-600 rounded-r outline-orange-400"
            />
          </div>
        </div>
        <button
          onClick={handleClick}
          className="bg-slate-900 text-white py-2 my-3 rounded"
        >
          View History
        </button>

        <div className="text-left flex flex-col">
          <span className="font-bold">Examples:</span>
          <Link
            href="/prabin-acharya/aqi-delhi"
            className="px-2 py-1 bg-slate-400 rounded mb-1 text-sm w-fit"
          >
            prabin-acharya/aqi-delhi
          </Link>
          <Link
            href="/prabin-acharya/nasa-image-of-the-day"
            className="px-2 py-1 bg-slate-400 rounded mb-1 text-sm w-fit"
          >
            prabin-acharya/nasa-image-of-the-day
          </Link>
        </div>
      </div>
    </main>
  );
}
