import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      backgroundColor: {
        fileContentBg: "#F8F8FF",
        arcLightBlue: "#C7C7FD",
        darkBlueMask: "#4338CA",
        arkRed: "#EB8891",
        roam: "#2D7DBD",
      },
      fontSize: {
        xxs: "0.5rem",
        logo: "1.58rem",
      },
    },
  },
  plugins: [],
};
export default config;
